import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

class TeamPickerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTeam: null,
    };
  }
  openHomeScreen() {

  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Wybierz drużynę</Text>
        <View style={styles.buttonsContainer}>
          <TouchableOpacity
            onPress={() => {
            }}
            style={[styles.grinchBtn, styles.btn]}>
            {/*<Image source={require('../../assets/grinch.jpg')} />*/}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {

            }}
            style={[styles.santaBtn, styles.btn]}>
            {/*<Image source={require('../../assets/santa.jpg')} />*/}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
    height: '100%',
  },
  btn: {
    justifyContent: 'center',
  },
  grinchBtn: {
    backgroundColor: '#009432',
    width: '50%',
    height: '100%',
  },
  santaBtn: {
    backgroundColor: '#eb2f06',
    width: '50%',
    height: '100%',
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    position: 'absolute',
    top: 10,
    zIndex: 10,
    elevation: 1,
    color: '#000',
    backgroundColor: '#fff',
    padding: 5,
    borderRadius: 10,
  },
});

export default TeamPickerScreen;
