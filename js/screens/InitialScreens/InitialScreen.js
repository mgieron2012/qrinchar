import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';


class InitialScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
    };
  }
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Qrinch</Text>
        <View style={styles.inputPanel}>
          <Text style={styles.input}>Nazwa użytkownika:</Text>
          <TextInput
            placeholder="ozon123"
            style={styles.input}
            onChangeText={text => this.setState({username: text})}
            value={this.state.username}
          />
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                navigate('TeamPicker', {username: this.state.username})
              }}>
              <Text style={styles.btnText}>Dołącz</Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    top: 10,
    fontSize: 40,
    marginBottom: 20,
    position: 'absolute',
  },
  inputPanel: {
    alignItems: 'center',
  },
  input: {
    fontSize: 20,
    textAlign: 'center',
  },
  btn: {
    width: 200,
    borderRadius: 15,
    alignItems: 'center',
    padding: 5,
    backgroundColor:'#78e08f',
  },
  btnText: {
    fontSize: 17,
    color: '#fff',
  },
});

// const mapDispatchToProps = {
//   setUsername: makeAction(SET_USERNAME),
// };

export default InitialScreen;
