import React, {Component} from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    Text,
    TouchableOpacity,
    Modal
} from 'react-native';
class InitializationModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
        }
    }

    render() {
        const {registerUser, isModalOpen} = this.props;
        return (
            <Modal
                visible={isModalOpen}
                transparent={false}
                animationType={"slide"}
            >
                <View style={styles.initializationModal}>
                    <TextInput
                        style={styles.usernameInput}
                        placeholder={"nazwa użytkownika"}
                        value={this.state.username}
                        onChangeText={(text) => this.setState({username: text})}
                    />
                    <View style={styles.teamButtonsContainer}>
                        <TouchableOpacity
                            onPress={() => registerUser(this.state.username, true)}
                            style={[styles.teamBtn, {backgroundColor: 'red'}]}
                        >
                            <Text style={styles.teamBtnText}>Drużyna mikołaja</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.teamBtn, {backgroundColor: 'green'}]}
                            onPress={() => registerUser(this.state.username, false)}
                        >
                            <Text style={styles.teamBtnText}>Drużyna Grinch'a</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
};

const styles = StyleSheet.create({
    initializationModal: {
        flex: 1,
        height: '100%',
        width: '100%',
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    usernameInput: {
        fontSize: 40,
        marginBottom: 40,
    },
    teamButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    teamBtn: {
        padding: 10,
        borderRadius: 20,
        margin: 5
    },
    teamBtnText: {
        color: '#fff',
        fontSize: 17,
    }
});

export default InitializationModal;
