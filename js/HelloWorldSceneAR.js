'use strict';

import React, { Component } from 'react';

import {
    StyleSheet,
    Alert
} from 'react-native';

import {
  ViroARScene,
  ViroConstants,
  Viro3DObject
} from 'react-viro';

export default class HelloWorldSceneAR extends Component {

  constructor() {
    super();

    // Set initial state here
    this.state = {
      text : "Initializing AR...",
      isGiftCollected: false,
    };

    // bind 'this' to functions
    this._onInitialized = this._onInitialized.bind(this);
  }

  render() {
    return (
      <ViroARScene onTrackingUpdated={this._onInitialized} >
        {
          !this.isGiftCollected && this.props.isPresentCloseEnough ?
              <Viro3DObject
                  source={require('./assets/gift.obj')}
                  position={[0, 0, 0]}
                  type={"OBJ"}
                  scale={[0.1, 0.1, 0.1]}
                  resources={[require('./assets/gift_re.mtl')]}
                  onClick={() => {
                    this.setState({isGiftCollected: true});
                    setTimeout(() => {
                      this.props.giftClicked();
                    }, 1000);
                  }}
              />
              : null
          }
      </ViroARScene>
    );
  }

  _onInitialized(state, reason) {
    if (state == ViroConstants.TRACKING_NORMAL) {
      this.setState({
        text : "Hello World!"
      });
    } else if (state == ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }
}

var styles = StyleSheet.create({

});

module.exports = HelloWorldSceneAR;
