import io from 'socket.io-client';

var toRad = function (num) {
    return num * Math.PI / 180
};

export function dist(obj1, obj2){
    start = obj1.coord;
    end = obj2.coord;

    var R = 6371;

    var dLat = toRad(end.latitude - start.latitude);
    var dLon = toRad(end.longitude - start.longitude);
    var lat1 = toRad(start.latitude);
    var lat2 = toRad(end.latitude);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    return R * c
}

class Data{
    constructor(){
        this.socket = io('http://89.38.148.169:4200');
        this.socket.on('registered',  data => this.onUserRegistered(data))
        this.socket.on('presentPickedupByPlayer',  data => this.presentPickedUp(data))
        this.socket.on('getPlayers',  data => this.getPlayers(data))
        this.socket.on('getPresentsAndPoints', data => this.getPresentsAndPoints(data))
        this.isSanta = false
        this.players = []
        this.id = -1
        this.presents_raw = []
        this.points = [0, 0]
        console.log(this.ownPlayers)
    }

    registerUser(username = "Zonk256", isSanta = false){
        this.socket.emit('registerUser', {
            name: username,
            team: isSanta
        })
        this.isSanta = isSanta
    }

    onUserRegistered({id, points, presents, players}){
        this.id = id
        this.players = players
        this.points = points
        this.presents_raw = presents
    }

    pickUpPresent({ id }){
        this.socket.emit('pickupPresent', {
            id: id,
            team: this.isSanta
        })
    }

    presentPickedUp({success, loot}){

    }

    getPresentsAndPoints({presents, points}){
        this.presents_raw = presents
        this.points = points
    }

    sendCoords(longitude, latitude){
        this.socket.emit('sendCoords', {
            id: this.id,
            longitude: longitude,
            latitude: latitude
        });
    }

    getPlayers({ players }){
        this.players = players
    }

    get ownPlayers() { return this.players.filter(({ team }) => team == this.isSanta) }
    get oppPlayers() { return this.players.filter(({ team }) => team != this.isSanta) }

    get presents() {
        presents_prep = []
        this.presents_raw.forEach( present => {
            min_op = 10000000
            min_own = 10000000
            this.oppPlayers.forEach( player => {
                distance = dist(player, present)
                min_op = distance < min_op ? distance : min_op
            })
            this.ownPlayers.forEach( player => {
                distance = dist(player, present)
                min_own = distance < min_own ? distance : min_own
            })
            present['r_own'] = min_own
            present['r_opp'] = min_op
            presents_prep.push(present)
        })
        return presents_prep
    }
}

export default Data;
