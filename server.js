var express = require('express');
var http = require('http');



console.log('Server started');
var app = express();
var serv = http.createServer(app);
console.log('Serwer działa na porcie: 4200');
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/data/index.html');
});

app.use(express.static('data'));

serv.listen(4200);

//zmienne globalne
var players = []; //id graczy zarejestrowanych
var presents = [] //informacje o prezentach
var points = {
  santa: 0,
  qrinch: 0
}; //punkty obu drużyn
var goal = 2400; //liczba prezentów jaką trzeba zebrać aby wygrać

//addLocalPresents();
addSciPresents();

var io = require('socket.io')(serv, {});

setInterval(function () {
  io.emit('getPlayers', {
    players: players
  });
}, 3000)

io.sockets.on('connection', function (socket) {
  console.log('data recived');

  socket.on('registerUser', function (data) { //zapisz nowego użytkownika
    var newId;
    while (true) {
      newId = randomString(8);
      if (!userExists(newId)) break;

    }

    players.push({
      id: newId,
      name: data.name,
      points: 0,
      team: data.team,
      coord: {
        longitude: -1.0,
        latitude: -1.0
      }
    });
    console.log(players);
    console.log(presents);
    socket.emit('registered', {
      id: newId,
      points: points,
      presents: presents,
      players: players.filter(({
        team
      }) => team == data.team)
    });
  });

  socket.on('updateStatus', function (data) { //nie wiem po co jesli z jakiegoś powodu chcesz odświeżyć dane
    socket.emit('status', {
      presents: presents,
    });
  });

  socket.on('sendCoords', function (data) { //
   // console.log('=====');
   // console.log(data);
  //  console.log('=====');
    var user = players.find(({
      id
    }) => id == data.id);
    if (user) {
      user.coord.longitude = data.longitude;
      user.coord.latitude = data.latitude;
    }
   // console.log(players);
  //  console.log('=====');

  });
  socket.on('getPlayers', function (data) { //nie wiem po co jesli z jakiegoś powodu chcesz odświeżyć dane
    socket.emit('getPlayers', players.filter(({
      team
    }) => team == data.team));
  });


  socket.on('pickupPresent', function (data) { //event mówiący o podniesieniu prezentu
    var present = presentExists(data.id);
    if (present) {
      //podnieś prezent, pamiętaj że to ID + 1
      present -= 1; //przywróć dobre ID
      var pointsFromPresent = presents[present].points; // ile punktów było w prezencie
      console.log("liczba punktów w prezencie: " + pointsFromPresent);
      //presents[present].points += pointsFromPresent;
      //TODO
      deletePresent(data.id); //usuń prezent z tablicy


      if (data.team) points.santa += pointsFromPresent;//dodaj punkty drużynie
      else points.qrinch += pointsFromPresent; 
      io.emit('presentPickuped', { //wysyła wiadomość do wszystkich graczy
        presents: presents,
        points: points
      });

      // socket.emit('presentPickuped', {presents: presents, points: points});
      socket.emit('pickupStatus', {
        success: true,
        loot: presents.find(({id}) => id == data.id).loot
      });
      

      console.log("Wynik: " + points.santa + " <> " + points.qrinch);
    } else {
      //ktoś musiał właśnie podnieść prezent albo złe ID
      socket.emit('pickupStatus', {
        success: false
      });
    }
  });
});


function addLocalPresents(numberOfPresents = 10) {
  console.log("Generuje prezenty w szkole...");
  for (var i = 0; i < numberOfPresents; i++) {
    var x = getRandomInt(-100, 100); //randomowe x coordy w zasięgu szkoły
    var y = getRandomInt(-100, 100); //randomowe x coordy w zasięgu szkoły
    addPresent(x, y);
  }
}


function addSciPresents() {
  addPresent(53.430761, 14.554912);
  //addPresent(53.430764, 14.554990);
  //addPresent(53.430722, 14.555254);
  //addPresent(53.430785, 14.555290);
  //addPresent(53.430798, 14.555233);
}


function addPresent(y = 0, x = 0, points = 50, loot = 'Cichoń') {
  var newId;
  while (true) {
    newId = randomString(5);
    if (!presentExists(newId)) break;
  }

  presents.push({
    id: newId,
    coord: {
      longitude: x,
      latitude: y
    },
    points: points,
    loot: loot
  });
}

function deletePresent(id) {
  for (var i = 0; i < presents.length; i++) {
    if (presents[i].id === id) {
      presents.splice(i, 1);
      return true;
    }
  }
  return false;
}

function presentExists(id) {
  for (var i = 0; i < presents.length; i++) {
    if (presents[i].id === id) return i + 1;
  }
  return false;
}

function userExists(id) {
  for (var i = 0; i < players.length; i++) {
    if (players[i].id === id) return true;
  }
  return false;
}


function randomString(len, bits) {
  bits = bits || 36;
  var outStr = "",
    newStr;
  while (outStr.length < len) {
    newStr = Math.random().toString(bits).slice(2);
    outStr += newStr.slice(0, Math.min(newStr.length, (len - outStr.length)));
  }
  return outStr.toUpperCase();
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}