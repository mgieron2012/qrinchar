/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import React from 'react';
import HomeScreen from "./js/screens/HomeScreen/HomeScreen";

const App = () => {
    return(
        <HomeScreen/>
    )
};

export default App;
